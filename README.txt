For question 1:

type "g++ -std=c++11 mcmc.cpp -o mcmc" to compile
type "./mcmc" to run
points generated during burn-in are output to "burn_in.txt"
points generated after brun-in are output to "convergence.txt"

to generate the plots shown in "CM APS9.pdf", open gnuplot and type:
plot 'burn_in.txt' with lines, 'convergence.txt' with lines

For question 2:

In order to enable fixing of the proposal function after burn in, go to line 15 of mcmc.cpp

At line 15, set q2flag = 1. This enables fixing. If set to 0, this disables fixing.
