//This is an MCMC routine for any multi-dimensional function using a product of gaussians as the proposal function
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <random>
#include <iomanip>

using namespace std;

/* ************************************** */

// Change this to 1 to adapt the standard deviation of the proposal distribution during convergence testing

const int q2flag=0;

/* ************************************** */

const double sigma_threshold=1e-9; //The maximum standard deviation required for the MCMC to converge
const double sigma_init=5.; //The initial sigma value
const int N=2; //Number of dimensions of function
const int M=100; //Number of points taken to calculate standard deviatons during burn-in period
const int O=1000; //Number of points in burn-in period
const int P=100; //Number of points taken to calculate standard deviations during convergence testing


mt19937 generator(time(0)); //seeds random number generator based on the current time
uniform_real_distribution<double> uniform(0.0,1.0); //Beta is drawn from this uniform distribution
uniform_real_distribution<double> start(-10.,10.); //Initial point in chain is drawn from this uniform distribution

//Uniform random number generator
double prng(){

	return uniform(generator);
}

//Gaussian random number generator
double gauss(double mu, double sigma){
    normal_distribution<double> normal(mu,sigma);
    return normal(generator);
}

//Function being optimised

double f(vector<double> x){
	return ((1.-x[0])*(1.-x[0]))+(100.*((x[0]*x[0])-x[1])*((x[0]*x[0])-x[1]));
}

//Generate random vector x from product of gaussians
vector<double> get_random_x(vector<double> mu, vector<double> sigma){

	vector<double> x(N);	
	for(int i =0;i<N;i++){
        x[i]=gauss(mu[i],sigma[i]);
	}
	return x;
}

//Metropolis hastings step
vector<double> metropolis_hastings(vector<double> x, vector<double> sigma, vector<double> mu){
    
    vector<double> z(N); //The vector that shall be returned
	vector<double> y(N);
	y=get_random_x(mu,sigma);
    z=x;
    double alpha=(f(y)/f(x));
    double beta;
    
    if(alpha<=1.){

        	z=y;

	}

	else{
        	beta = prng();
       		if(beta <= (1./alpha)){
        	z=y;
        	}
     
	}	
     

	return z;

}

//Calculates the mean of the first 'I' vectors in the matrix X (more comments later on what the matrix X is)
vector<double> mean(vector< vector<double> > X, int I){
    vector<double> mu(N);
    double temp=0.;
    for(int i=0;i<N;i++){
        for(int j=0;j<I;j++){
            temp=X[j][i]+temp; //For a given component i, sum up all the ith components of the first I vectors in the matrix X
        }
        mu[i]=temp/I; //Divide by I to calculate the mean
        temp=0.;
    }
    return mu;
}

//Calculates the standard deviation of the firt 'I' vectors in the matrix X
vector<double> standard_dev(vector< vector<double> > X, vector<double> mu, int I){
    vector<double> sigma(N);
    double temp=0.;
    for(int i=0;i<N;i++){
        for(int j=0;j<I;j++){
            temp=((X[j][i]-mu[i])*(X[j][i]-mu[i]))+temp; //Same logic as the summing for the mean function above
        }
        sigma[i]=sqrt(temp/I); //Divide by I then take the square root to calculate the standard deviation
        temp=0.;
    }
    return sigma;
}

//Function for testing whether each component of the input standard deviation is smaller than the threshold standard deviation
int convergence(vector<double> sigma){
    vector<int> con(N);
    int test=1;
    for(int i=0;i<N;i++){
        if(abs(sigma[i])<sigma_threshold){
            con[i]=1;
        }
        else{
            con[i]=0;
        }
        test=con[i]*test;
    }

    return test; //If test =1 , then all the components of sigma were smaller than sigma_threshold, otherwise if test=0, they weren't
}

/* ********** THE MAIN FUNCTION *********** */

int main(){

	vector<double> x(N); //Define vector which moves from point to point in the chain
    vector<double> mu(N), sigma(N); //Define the mean and standard deviation 'vectors' for the components of each vector
    vector< vector<double> > x_points(M,vector<double>(N)); //Define the matrix which stores all vectors used at each point in chain
    
/*  The matrix x_points is constructued as follows. There are M rows and N columns. Each row corresponds to an N-dimensional vector. For example, x_points[0] is the first row (or vector) in the matrix. Each column corresponds to the respective component of the vector. For example, x_points[0][1] is the 2nd component of the first vector in the matrix. */
    
	ofstream burn("burn_in.txt"); //Open burn-in data file
    ofstream data("convergence.txt"); //Open convergence testing period data file
    
    cout << "  " << endl;
    
    if(q2flag==0){
        cout << "Question 1: No fixing of standard deviation after burn-in." << endl;
    }
    
    else{
        cout << "Question 2: Fixing standard deviation after burn-in." << endl;
    }
    
    cout << "  " << endl;
    
    cout << "Starting point: ";
    for(int i=0;i<N;i++){
        x[i]=start(generator);
        cout << x[i] << "\t";
    }
    cout << endl;
    
    
        // Writes starting point to burn_in.txt
 	       for(int i=0;i<N;i++){
               burn << x[i] << "\t";
           }
            burn << endl;
    
    x_points[0]=x; //Stores vector in first row of data matrix
    
    vector<double> mu_x(N); //The mean 'vector' of the previous M points
    mu=x; //mu is the centre of the proposal function
    mu_x=mean(x_points,1);
    
    //Initialises the standard deviations of the proposal function
    for(int i=0;i<N;i++){
        sigma[i]=sigma_init;
    }
    
    int a=0;
    int b=1;
    int burn_count = 1; //Measures how long burn-in is
    
//Burn in period
    for(int i=0;i<O;i++){ //Burn-in is O (defined at the beginning of code) steps long
    //while(convergence(sigma)==0){ //Comment back in this line and comment out the previous line to make the length of burn-in adaptive
        
        x=metropolis_hastings(x,sigma,mu); //Perform metropolis hastings step

                //Write new vector to burn_in.txt
                for(int j=0;j<N;j++){
                    burn << setprecision(20) << x[j] << "\t";
                }

                burn << endl;
    
        if(a==0){ //Until we reach the Mth row (i.e. the last row) base the standard deviation on the previous 'b' points
            x_points[b]=x;
        
            mu=x;
        
            mu_x=mean(x_points,b+1);
            sigma=standard_dev(x_points,mu_x,b+1);
            for(int j=0;j<N;j++){
                sigma[j]=sigma[j]+(sigma_init/(b+1)); //This formula prevents the initial standard deviations from becoming zero too quickly
            }
        }
        
        if(a==1){
            x_points[b]=x;
            
            mu=x;
            
            mu_x=mean(x_points,M);
            sigma=standard_dev(x_points,mu_x,M);
        }

        b++;
        
        burn_count++;
        
        if(b==M){
            b=0;
            a=1;
        }
	}
   
    cout << "Number of steps in burn-in is : " << burn_count << endl;
    
    vector<double> best(N);
    best = x; //'best' is the vector that returns the lowest values of f(x)

    vector< vector<double> > x_point(P,vector<double>(N)); //Define a new matrix which is P rows long
    
    x_point[0]=x;
    
    vector<double> mu_c(N);
    vector<double> sigma_current(N);
    
    
    mu_c=mean(x_points,1);
    for(int i=0;i<N;i++){
        sigma_current[i]=2.; //Stop standard deviation from becoming zero too quickly
    }

    int k=1;
    int l=0; //This flag serves the same purpose as the 'a' flag in the burn-in period
    int chain_length=1; //This variable calculates how long the chain after burn-in is
    
//Metropolis Hastings steps after burn-in
    while( (convergence(sigma_current)==0) ){
        
        if(q2flag==1){ //Notice that if q2flag==1, then sigma changes every step in this while loop hence adapting the proposal function at every step after burn-in
            sigma=sigma_current;
        }
        
        x=metropolis_hastings(x,sigma,mu);
	
            for(int i=0;i<N;i++){ //Write to convergence.txt
                data << setprecision(20) << x[i] << "\t";
            }
            data << endl;
        mu=x;
        x_point[k]=x;
        
        if(f(x_point[k])<f(best)){
            best = x_point[k];
        }
        
        if(l==0){
            mu_c=mean(x_point,k+1);
            
            sigma_current=standard_dev(x_point,mu_c,k+1);
            for(int i=0;i<N;i++){
                sigma_current[i]=sigma_current[i]+(2./(k+1));
            }
            
        }
        
        if(l==1){
            mu_c=mean(x_point,P);
            
            sigma_current=standard_dev(x_point,mu_c,P);
            
        }
        
        k++;
        chain_length++;
        
        if(k==P){ //Prevents a segmentation fault
            k=0;
            l=1;
        }

	}
    
    
    cout << "The minimising vector x_min is ";
    
    for(int i=0; i<N;i++){
        cout<<best[i]<<"\t";
    }
    cout << endl;
    
    cout << "f(x_min) = " << f(best) << endl;
    cout << "The length of the chain is " << chain_length << " points." << endl;
    cout << "  " << endl;
    
return 0;
}


